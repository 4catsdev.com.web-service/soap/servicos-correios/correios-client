# correios-client
Um exemplo de consulta de endereço de uma localidade através do CEP. A aplicação utiliza o serviço SOAP fornecido pelos Correios. Em sua página principal, a aplicação disponibiliza um formulário no qual o usuário informa o CEP e recebe como resultado o endereço correspondente.

Vídeo no youtube: https://youtu.be/2fShc17eoKI

#### Características:
- A requisição entre o JSP e Servlet é feita por Ajax;
- É usado o jaxws-maven-plugin para gerar as classes dos correios no cliente;
- É usado a versão 3.0.0 do jax-ws
