package com.fourcatsdev.correiosweb.servico.controle;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fourcatsdev.correiosweb.servico.AtendeCliente;
import com.fourcatsdev.correiosweb.servico.EnderecoERP;
import com.fourcatsdev.correiosweb.servico.util.Conexao;


@WebServlet("/CorreiosControle")
public class CorreiosControle extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	private AtendeCliente atendeCliente;
	
    public CorreiosControle() {
        super();
        atendeCliente = Conexao.getServicocorreio();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 // Obter o valor do parâmetro "cep" enviado pelo AJAX
        String cep = request.getParameter("cep");
        
        String enderecoResposta = "";
		try {
			EnderecoERP endereco = atendeCliente.consultaCEP(cep);
			enderecoResposta = endereco.getEnd() + ", " +					
					     endereco.getBairro() + ", " +
					     endereco.getCidade() + "-" + endereco.getUf();						
		} catch (Exception e) {
			enderecoResposta = "Endereço não existe";
		}
		
		 // Definir o tipo de conteúdo da resposta
        response.setContentType("text/plain");
        
        // Enviar o endereço como resposta
        response.getWriter().write(enderecoResposta);
        
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
